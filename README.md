# projet-tuteure-s4-api

1) Modifier les identifiants dans le Dockerfile ET dans le docker-compose.yml

2) Construction de l'image Docker de la BDD :
docker build -t mariadb-with-data .

3) Lancement des conteneurs de l'API :
docker-compose up -d
